import PropTypes from "prop-types";
import React from "react";
import { FormGroup, Input, Label, FormFeedback } from "reactstrap";
import { ErrorMessage } from "formik";

InputField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
};

InputField.defaultProps = {
  type: "text",
  label: "",
  placeholder: "",
  disabled: false,
};

function InputField(props) {
  const { field, form, type, label, placeholder, disabled } = props;
  //   const { name, value, onChange, onBlur } = field;
  const { name } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];
  return (
    <FormGroup>
      {/* ES6 */}
      {label && <Label for={name}>{label}</Label>}
      <Input
        id={name}
        {...field}
        // tuong duong
        // name={name}
        // value={value}
        // onChange={onChange}
        // onBlur={onBlur}
        type={type}
        disabled={disabled}
        placeholder={placeholder}
        invalid={showError} // phải có mới dùng đc formfeedback của reactstrap va ErrorMessage của formik(van la dung component formfeedback) <-> className="is-invalid"
      />

      {/* Xử lý thông thường */}
      {/* {showError && <FormFeedback>{errors[name]}</FormFeedback>} */}

      {/* Dùng ErrorMessage của Formik */}
      <ErrorMessage name={name} component={FormFeedback} />
    </FormGroup>
  );
}

export default InputField;
