import PropTypes from "prop-types";
import React from "react";
import { FormGroup, Label, FormFeedback } from "reactstrap";
import Select from "react-select";
import { ErrorMessage } from "formik";

SelectField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  options: PropTypes.array,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
};

SelectField.defaultProps = {
  options: [],
  label: "",
  placeholder: "",
  disabled: false,
};

function SelectField(props) {
  const { field, form, label, placeholder, options, disabled } = props;
  //   const { name, value, onChange, onBlur } = field;
  const { name, value } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];

  const selectedOption = options.find((option) => option.value === value);

  const handleSelectedOptionChange = (selectedOption) => {
    const selectedValue = selectedOption
      ? selectedOption.value
      : selectedOption;

    const changeEvent = {
      target: {
        name: name,
        value: selectedValue,
      },
    };
    field.onChange(changeEvent);
  };

  return (
    <FormGroup>
      {/* ES6 */}
      {label && <Label for={name}>{label}</Label>}
      <Select
        id={name}
        {...field}
        // tuong duong
        // name={name}
        value={selectedOption}
        onChange={handleSelectedOptionChange}
        // onBlur={onBlur}
        options={options}
        isDisabled={disabled}
        placeholder={placeholder}
        // Khong hỗ trợ sẵn invalid nên phải tự định nghĩa
        className={showError ? "is-invalid" : ""}
      />
      <ErrorMessage name={name} component={FormFeedback} />
    </FormGroup>
  );
}

export default SelectField;
