import { unwrapResult } from "@reduxjs/toolkit";
import ProductApi from "api/productApi";
import { getMe } from "app/userSlice";
import SignIn from "features/Auth";
import firebase from "firebase";
import React, { Suspense, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import "./App.css";
import Header from "./components/Header";
import NotFound from "./components/NotFound";

// lazy load - code splitting
const Photo = React.lazy(() => import("./features/Photo"));

// Configure Firebase.
const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  // ...
};
firebase.initializeApp(config);

function App() {
  const [productList, setProductList] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchProductList = async () => {
      try {
        const params = {
          _page: 1,
          _limit: 10,
        };
        const response = await ProductApi.getAll(params);
        console.log(response);
        setProductList(response.data);
      } catch (error) {
        console.log("Failed to fetch api: ", error);
      }
    };

    fetchProductList();
  }, []);

  // handle firebase auth changed
  useEffect(() => {
    const unregisterAuthObserver = firebase
      .auth()
      .onAuthStateChanged(async (user) => {
        if (!user) {
          // user logs out, handle something here
          console.log("User is not logged in");
          return;
        }
        // Get me when signed in (to redux)

        try {
          const actionResult = await dispatch(getMe());
          const currentUser = unwrapResult(actionResult); // catch dc ca error
          console.log("Logged in user: ", currentUser);
        } catch (error) {
          console.log(error);
        }

        // console.log("Logged in user: ", user.displayName);
        // const token = await user.getIdToken();
        // console.log("Logged in token ", token);
      });
    return () => {
      unregisterAuthObserver();
    };
  }, []);

  return (
    <div className="photo-app">
      <Suspense fallback={<div>Loading ...</div>}>
        <BrowserRouter>
          <Header />
          <Switch>
            <Redirect exact from="/" to="/photos" />
            <Route path="/photos" component={Photo} />
            <Route path="/sign-in" component={SignIn} />
            <Route component={NotFound} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </div>
  );
}

export default App;
