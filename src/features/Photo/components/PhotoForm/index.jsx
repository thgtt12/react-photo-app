import PropTypes from "prop-types";
import InputField from "custom-fields/InputField";
import { FastField, Form, Formik } from "formik";
import React from "react";
import SelectField from "custom-fields/SelectField";
import { PHOTO_CATEGORY_OPTIONS } from "constants/global";
import RandomPhotoField from "custom-fields/RandomPhotoField";
import { FormGroup, Button, Spinner } from "reactstrap";
import * as Yup from "yup";

PhotoForm.propTypes = {
  onSubmit: PropTypes.func,
};

PhotoForm.defaultProps = {
  onSubmit: null,
};

function PhotoForm(props) {
  const { initialValues, isAddMode } = props;

  // const initialValues = {
  //   title: "",
  //   categoryId: null,
  //   photo: "",
  // };

  const validateSchema = Yup.object().shape({
    title: Yup.string().required("This field is required"),

    categoryId: Yup.number().required("This field is required").nullable(), // ho tro ca gia tri null, cho phep null

    photo: Yup.string().when("categoryId", {
      is: 1,
      then: Yup.string().required("This field is required"),
      otherwise: Yup.string().notRequired(),
    }),
  });

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={props.onSubmit}
      validationSchema={validateSchema}
    >
      {(formikProps) => {
        // do something here
        const { values, errors, touched, isSubmitting } = formikProps;
        console.log({ values, errors, touched });
        return (
          <Form>
            <FastField
              // props cua fastfield
              name="title"
              component={InputField}
              // -------------------

              // props cua InputField
              label="Title"
              placeholder="Eg: Wow nature ..."
            ></FastField>

            <FastField
              name="categoryId"
              component={SelectField}
              // ---------------
              label="Category"
              placeholder="What yours photo category?"
              options={PHOTO_CATEGORY_OPTIONS}
            ></FastField>

            <FastField
              name="photo"
              component={RandomPhotoField}
              // ---------------
              label="Photo"
            ></FastField>
            <FormGroup>
              <Button type="Submit" color={isAddMode ? "primary" : "success"}>
                {isSubmitting && <Spinner size="sm" />}
                {isAddMode ? "Add to album" : "Update your photo"}
              </Button>
            </FormGroup>
          </Form>
        );
      }}
    </Formik>
  );
}

export default PhotoForm;
