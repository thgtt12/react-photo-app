import userApi from "api/userApi";

const { createSlice, createAsyncThunk } = require("@reduxjs/toolkit");

// tao async action
export const getMe = createAsyncThunk(
  "user/getMe",
  async (params, thunkAPI) => {
    // thunkAPI.dispatch(...) neu muon dispatch 1 action khac
    const currentUser = await userApi.getMe();
    return currentUser; // -> chinh la payload o dong 35 // cung co the lay ra bang unwrapResult giong ben App.js
  }
);

const userSlice = createSlice({
  name: "user",
  initialState: {
    current: {},
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: {
    [getMe.pending]: (state) => {
      // khi bat dau
      state.loading = true;
    },
    [getMe.rejected]: (state, action) => {
      // khi bao loi
      state.loading = false;
      state.error = action.error;
    },
    [getMe.fulfilled]: (state, action) => {
      state.loading = false;
      state.current = action.payload;
    },
  },
});

const { reducer: userReducer } = userSlice;
export default userReducer;
